import { Component, OnInit } from '@angular/core';
import {Post} from "../../models/post.model";
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";
import {AppState} from "../store/types";
import {fetchPostsRequest} from "../store/posts.action";
import {User} from "../../models/user.model";

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.sass']
})
export class PostsComponent implements OnInit {
  posts: Observable<Post[]>
  user: Observable<User | null>
  loading: Observable<boolean>
  error: Observable<null | string>


  constructor(private store: Store<AppState>) {
    this.posts = store.select(state => state.posts.posts);
    this.user = store.select(state => state.users.user)
    this.loading = store.select(state => state.posts.fetchLoading);
    this.error = store.select(state => state.posts.fetchError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchPostsRequest());

  }
}
