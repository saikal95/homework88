import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import {Store} from "@ngrx/store";
import {User} from "../../../models/user.model";
import {AppState} from "../../store/types";
import {logoutUser, logoutUserRequest} from "../../store/users.action";

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})

export class LayoutComponent {


  user: Observable<null | User>;

  constructor(private breakpointObserver: BreakpointObserver,
  private store: Store<AppState>) {
    this.user = store.select(state => state.users.user);
  }


  logout(){
    this.store.dispatch(logoutUser());
  }
}
