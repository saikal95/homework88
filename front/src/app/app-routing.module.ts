import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RegisterComponent} from "./pages/register/register.component";
import {LoginComponent} from "./pages/login/login.component";
import {PostsComponent} from "./posts/posts.component";
import {AddNewComponent} from "./add-new/add-new.component";
import {OnePostComponent} from "./one-post/one-post.component";

const routes: Routes = [
  {path: '', component: PostsComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'posts/new', component: AddNewComponent},
  {path: 'posts/:id', component: OnePostComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
