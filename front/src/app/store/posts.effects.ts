import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {
  createPostFailure,
  createPostRequest,
  createPostSuccess,
  fetchPostFailure, fetchPostRequest,
  fetchPostsRequest,
  fetchPostsSuccess, fetchPostSuccess
} from "./posts.action";
import {catchError, mergeMap, of, tap} from "rxjs";
import {PostsService} from "../services/posts.service";
import {map} from "rxjs/operators";
import {Router} from "@angular/router";
import {fetchCommentsFailure, fetchCommentsRequest, fetchCommentsSuccess} from "./comments.action";


@Injectable()
export class PostsEffects {

  constructor(
    private actions: Actions,
    private postService: PostsService,
    private router: Router
  ) {}
  fetchPosts = createEffect(() => this.actions.pipe(
    ofType(fetchPostsRequest),
    mergeMap(() => this.postService.getPosts().pipe(
      map(posts => fetchPostsSuccess({posts})),
      catchError(() => of(fetchPostFailure({
        error: 'Something went wrong'
      })))
    ))
  ));

  createPost = createEffect(() => this.actions.pipe(
    ofType(createPostRequest),
    mergeMap(({postData}) => this.postService.createPost(postData).pipe(
      map(() => createPostSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createPostFailure({error: 'Wrong data'})))
    ))
  ));

  fetchPost = createEffect(() => this.actions.pipe(
    ofType(fetchPostRequest),
    mergeMap(({id}) => this.postService.getPost(id).pipe(
      map(post => fetchPostSuccess({post})),
      catchError(() => of(fetchPostFailure({
        error: 'Something went wrong'
      })))
    ))
  ));




}
