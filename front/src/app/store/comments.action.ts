import {createAction, props} from "@ngrx/store";
import {Comment, CommentData} from "../../models/comment.model";



export const fetchCommentsRequest = createAction('[Comments] fetch Comment',
  props<{id: string}>() )

export const fetchCommentsSuccess = createAction('[Comments] fetch Success',
  props<{comments: Comment[]}>()
)
export const fetchCommentsFailure = createAction('[Comments] fetch Failure',
  props<{error: string}>()
);


export const createCommentRequest = createAction(
  '[Posts] Create Request',
  props<{commentData: CommentData, token: string}>()
);
export const createCommentSuccess = createAction(
  '[Posts] Create Success'
);
export const createCommentFailure = createAction(
  '[Posts] Create Failure',
  props<{error: string}>()
);
