import {createAction, props} from "@ngrx/store";
import {Post, PostData} from "../../models/post.model";


export const fetchPostsRequest = createAction('[Posts] fetch Request')

export const fetchPostRequest = createAction('[Posts] fetch Request',
  props<{id: string}>()
  )

export const fetchPostsSuccess = createAction('[Posts] fetch Success',
  props<{posts: Post[]}>()
)


export const fetchPostSuccess = createAction('[Posts] fetch Success',
  props<{post: Post}>()
)

export const fetchPostsFailure = createAction('[Posts] fetch Failure',
  props<{error: string}>()
)
export const fetchPostFailure = createAction('[Posts] fetch Failure',
  props<{error: string}>()
);


export const createPostRequest = createAction(
  '[Posts] Create Request',
  props<{postData: PostData}>()
);
export const createPostSuccess = createAction(
  '[Posts] Create Success'
);
export const createPostFailure = createAction(
  '[Posts] Create Failure',
  props<{error: string}>()
);
