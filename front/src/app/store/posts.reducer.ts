import {PostState, UserState} from "./types";
import {createReducer, on} from "@ngrx/store";
import {
  createPostFailure,
  createPostRequest,
  createPostSuccess,
  fetchPostFailure, fetchPostRequest,
  fetchPostsRequest,
  fetchPostsSuccess, fetchPostSuccess
} from "./posts.action";
import {Post} from "../../models/post.model";
import {fetchCommentsSuccess} from "./comments.action";



const initialState: PostState = {
  posts: [],
  post: null,
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
};

export const postsReducer = createReducer(
  initialState,
  on(fetchPostsRequest, state => ({...state, fetchLoading: true})),
  on(fetchPostsSuccess, (state, {posts}) => ({
    ...state,
    fetchLoading: false,
    posts
  })),
  on(fetchPostFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  })),
  on(createPostRequest, state => ({...state, createLoading: true})),
  on(createPostSuccess, state => ({...state, createLoading: false})),
  on(createPostFailure, (state, {error}) => ({
    ...state,
    createLoading: false,
    createError: error,
  })),
  on(fetchPostRequest, (state) => ({
    ...state,
    fetchLoading: false,
  })),
  on(fetchPostSuccess, (state, {post}) => ({
    ...state,
    fetchLoading: false,
    post
  })),
  on(fetchPostFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  })),
);
