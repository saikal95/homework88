import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Router} from "@angular/router";
import {catchError, mergeMap, of, tap} from "rxjs";
import {map} from "rxjs/operators";
import {CommentsService} from "../services/comments.service";
import {
  createCommentFailure,
  createCommentRequest, createCommentSuccess,
  fetchCommentsFailure,
  fetchCommentsRequest,
  fetchCommentsSuccess
} from "./comments.action";

@Injectable()
export class CommentsEffects {

  constructor(
    private actions: Actions,
    private commentService: CommentsService,
    private router: Router
  ) {}
  fetchComments = createEffect(() => this.actions.pipe(
    ofType(fetchCommentsRequest),
    mergeMap(({id}) => this.commentService.getComments(id).pipe(
      map(comments => fetchCommentsSuccess({comments})),
      catchError(() => of(fetchCommentsFailure({
        error: 'Something went wrong'
      })))
    ))
  ));

  createComment= createEffect(() => this.actions.pipe(
    ofType(createCommentRequest),
    mergeMap(({commentData, token}) => this.commentService.createComment(commentData, token).pipe(
      map(() => createCommentSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createCommentFailure({error: 'You have inserted wrong data!!!'})))
    ))
  ));




}
