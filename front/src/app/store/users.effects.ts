import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {UserService} from "../services/user.service";
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Store} from "@ngrx/store";
import {AppState} from "./types";
import {
  loginUserFailure,
  loginUserRequest,
  loginUserSuccess, logoutUser,
  registerUserFailure,
  registerUserRequest,
  registerUserSuccess
} from "./users.action";
import {HelpersService} from "../services/helpers.service";
import { map, mergeMap, NEVER, tap, withLatestFrom } from 'rxjs';

@Injectable()
export class UsersEffects {
  constructor(
    private actions: Actions,
    private usersService: UserService,
    private router: Router,
    private snackbar: MatSnackBar,
    private helpers: HelpersService,
    private store: Store<AppState>
  ) {}

  registerUser = createEffect(()=> this.actions.pipe(
    ofType(registerUserRequest),
    mergeMap(({userData}) => this.usersService.registerUser(userData).pipe(
      map(user => registerUserSuccess({user})),
      tap(()=> {
        this.helpers.openSnackbar('Register Successful');
        void this.router.navigate(['/']);
      }),
      this.helpers.catchServerError(registerUserFailure)
    ))
  ))


  loginUser = createEffect(()=> this.actions.pipe(
    ofType(loginUserRequest),
    mergeMap(({userData}) => this.usersService.login(userData).pipe(
      map(user => loginUserSuccess({user})),
      tap(()=> {
        this.helpers.openSnackbar('Login successful');
        void this.router.navigate(['/']);
      }),
      this.helpers.catchServerError(loginUserFailure)
    ))
  ))



  logoutUser = createEffect(()=> this.actions.pipe(
    ofType(logoutUser),
    withLatestFrom(this.store.select(state => state.users.user)),
    mergeMap(([action, user])=> {
      if(user){
        return this.usersService.logout(user.token).pipe(
          map(()=> logoutUser()),
          tap(()=> this.helpers.openSnackbar('Logout successful')),
        )
      }

      return NEVER;

    })
  ))

}
