import {CommentState} from "./types";
import {createReducer, on} from "@ngrx/store";
import {
  createPostFailure,
  createPostRequest,
  createPostSuccess,
  fetchPostFailure,
  fetchPostsRequest,
  fetchPostsSuccess
} from "./posts.action";
import {
  createCommentFailure,
  createCommentRequest, createCommentSuccess,
  fetchCommentsFailure,
  fetchCommentsRequest,
  fetchCommentsSuccess
} from "./comments.action";


const initialState: CommentState = {
  comments: [],
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
};

export const commentsReducer = createReducer(
  initialState,
  on(fetchCommentsRequest, state => ({...state, fetchLoading: true})),
  on(fetchCommentsSuccess, (state, {comments}) => ({
    ...state,
    fetchLoading: false,
    comments
  })),
  on(fetchCommentsFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  })),
  on(createCommentRequest, state => ({...state, createLoading: true})),
  on(createCommentSuccess, state => ({...state, createLoading: false})),
  on(createCommentFailure, (state, {error}) => ({
    ...state,
    createLoading: false,
    createError: error,
  }))
);
