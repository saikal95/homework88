import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiPostData, Post, PostData} from "../../models/post.model";
import {environment} from "../../environments/environment";
import {map} from "rxjs/operators";
import {ApiCommentData, Comment} from "../../models/comment.model";

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private http: HttpClient) { }


  getPosts() {
    return this.http.get<ApiPostData[]>(environment.apiUrl + '/posts').pipe(
      map(response => {
        return response.map(postData => {
          return new Post(
            postData._id,
            postData.user,
            postData.title,
            postData.description,
            postData.image,
            postData.date,
            postData.token,
          );
        });
      })
    );
  }

  createPost(postData: PostData) {
    const formData = new FormData();

    Object.keys(postData).forEach(key => {
      if (postData[key] !== null) {
        formData.append(key, postData[key]);
      }
    });

    return this.http.post(environment.apiUrl + '/posts', formData);
  }


  getPost(id: string) {
    return this.http.get<Post>(environment.apiUrl + '/posts/' + id).pipe(
      map(response => {
        return response;
        })
      )

  }




}



