import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Post} from "../../models/post.model";
import {Observable, Subscription} from "rxjs";
import {Store} from "@ngrx/store";
import {AppState} from "../store/types";
import {ActivatedRoute} from "@angular/router";
import {Comment, CommentData} from "../../models/comment.model";
import {User} from "../../models/user.model";
import {fetchPostRequest} from "../store/posts.action";
import {createCommentRequest, fetchCommentsRequest} from "../store/comments.action";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-one-post',
  templateUrl: './one-post.component.html',
  styleUrls: ['./one-post.component.sass']
})
export class OnePostComponent implements OnInit, OnDestroy {
  @ViewChild('f') form! : NgForm;
  post: Observable<null | Post>;
  comments:Observable<Comment[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  user: Observable<null | User>;
  myUserSubscription!: Subscription;
  myPostSubscription!: Subscription;
  myPost!: Post | null;
  token!: string;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.post = store.select(state => state.posts.post)
    this.comments = store.select(state => state.comments.comments);
    this.loading = store.select(state => state.comments.fetchLoading);
    this.error = store.select(state => state.comments.fetchError);
    this.user = store.select(state => state.users.user);

  }


  ngOnInit(): void {
    this.store.dispatch(fetchPostRequest({id: this.route.snapshot.params['id']}));
    this.store.dispatch(fetchCommentsRequest({id: this.route.snapshot.params['id']}));

    this.myUserSubscription = this.user.subscribe(person=>{
        if(person){
          this.token = person.token
        } else {
          this.token = '';
        }

    })

    this.myPostSubscription = this.post.subscribe(post=>{
      if(post){
        this.myPost = post
      } else {
        this.myPost = null;
      }
    })

  }

  onSubmit(){
    const commentData : CommentData = {
      text: this.form.form.value.text,
      post: this.route.snapshot.params['id'],
    }

    const token = this.token;
    this.store.dispatch(createCommentRequest({commentData, token}));
    this.store.dispatch(fetchCommentsRequest({id: this.route.snapshot.params['id']}));

  }

  ngOnDestroy() {
    this.myPostSubscription.unsubscribe();
    this.myUserSubscription.unsubscribe();
  }

}
