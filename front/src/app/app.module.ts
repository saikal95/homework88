import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {ActionReducer, MetaReducer, StoreModule} from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {LayoutComponent} from "./ui/layout/layout.component";
import {FileInputComponent} from "./ui/file-input/file-input.component";
import {CenterCardComponent} from "./ui/center-card/center-card.component";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatListModule} from "@angular/material/list";
import {MatButtonModule} from "@angular/material/button";
import {FlexModule} from "@angular/flex-layout";
import {MatIconModule} from "@angular/material/icon";
import {MatMenuModule} from "@angular/material/menu";
import {FormsModule} from "@angular/forms";
import {MatCardModule} from "@angular/material/card";
import {ImagePipe} from "./pipes/image.pipe";
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {usersReducer} from "./store/users.reducer";
import {UsersEffects} from "./store/users.effects";
import {HttpClientModule} from "@angular/common/http";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import { localStorageSync } from 'ngrx-store-localstorage';
import { PostsComponent } from './posts/posts.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {postsReducer} from "./store/posts.reducer";
import {PostsEffects} from "./store/posts.effects";
import { AddNewComponent } from './add-new/add-new.component';
import {commentsReducer} from "./store/comments.reducer";
import {CommentsEffects} from "./store/comments.effects";
import { OnePostComponent } from './one-post/one-post.component';


const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true
  })(reducer);
}

const metaReducers : Array<MetaReducer> = [localStorageSyncReducer];

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    FileInputComponent,
    CenterCardComponent,
    ImagePipe,
    LoginComponent,
    RegisterComponent,
    PostsComponent,
    AddNewComponent,
    OnePostComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        StoreModule.forRoot({users: usersReducer, posts: postsReducer, comments: commentsReducer}, {metaReducers}),
        EffectsModule.forRoot([UsersEffects, PostsEffects, CommentsEffects]),
        MatSidenavModule,
        MatToolbarModule,
        MatListModule,
        HttpClientModule,
        MatButtonModule,
        FlexModule,
        MatIconModule,
        MatMenuModule,
        FormsModule,
        MatCardModule,
        MatSnackBarModule,
        MatFormFieldModule,
        MatInputModule,
        MatProgressSpinnerModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
