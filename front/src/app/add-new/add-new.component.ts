import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";
import {AppState} from "../store/types";
import {PostData} from "../../models/post.model";
import {createPostRequest} from "../store/posts.action";

@Component({
  selector: 'app-add-new',
  templateUrl: './add-new.component.html',
  styleUrls: ['./add-new.component.sass']
})
export class AddNewComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<string | null>;

  constructor(
    private store: Store<AppState>
  ) {
    this.loading = store.select(state => state.posts.createLoading);
    this.error = store.select(state => state.posts.createError);
  }

  ngOnInit(): void {
  }

  onSubmit() {
    const postData: PostData = this.form.value;
    this.store.dispatch(createPostRequest({postData}));
    console.log(postData);
  }
}
