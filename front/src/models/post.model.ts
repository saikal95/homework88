import {User} from "./user.model";

export class Post {
  constructor(
    public id: string,
    public user: User,
    public title: string,
    public description: string,
    public image: string,
    public date: string,
    public token: string
  ) {}
}

export interface PostData {
  [key: string]: any;
  user: User;
  title: string;
  description: string;
  image: File | null;
}

export interface ApiPostData {
  _id: string,
  user: User,
  title: string,
  description: string;
  image: string;
  date: string;
  token: string
}
