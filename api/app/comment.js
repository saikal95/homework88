const express = require('express');
const mongoose = require("mongoose");
const Comment = require("../models/Comment");
const auth = require("../middleware/auth");
const router = express.Router();


router.get('/', async (req, res, next) => {
  try {
    const query = {};

    if(req.query.post){
      query.post = req.query.post;
    }

    const comments = await Comment.find(query);

    return res.send(comments);
  } catch (e) {
    next(e);
  }
});

router.post('/', auth, async(req, res, next)=> {
  try{
    if (!req.body.text) {
      return res.status(400).send({error: 'You need to insert a text !'});
    }

    const commentData = req.body;
    commentData.user = req.user._id;

    const comment = new Comment(commentData);

    await comment.save();

    return res.send( {message: "New comment is created", id: post._id});

  }catch(e){
    next(e);
  }

})

module.exports = router;




