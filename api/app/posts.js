const express = require('express');
const mongoose = require("mongoose");
const Post = require("../models/Post");
const multer = require('multer');
const config = require('../config');
const path = require("path");
const {nanoid} = require('nanoid');
const auth = require("../middleware/auth");
const date = new Date().toISOString();

const router = express.Router();
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.post('/',auth, upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.title) {
      return res.status(400).send({message: 'Title of the post is required'});
    }

    if (!req.body.description && !req.body.image) {
      return res.status(400).send({message: 'Title of the post is required'});
    }

    const postData = {
      user: req.user._id,
      title: req.body.title,
      description: req.body.description,
      date: date,
      image: null,
    };

    if (req.file) {
      postData.image = req.file.filename;
    }

    const post = new Post(postData);

    await post.save();

    return res.send({message: 'Created new post', id: post._id});
  } catch (e) {
    next(e);
  }
});



router.get('/', async (req, res, next) => {
  try {

    const posts = await Post.find().populate("user", "name");

    return res.send(posts);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {

    const post = await Post.findById(req.params.id).populate("user", "name");
    console.log(post);
    if(!post){
      return res.status(404).send({message: 'Not found'});
    }
    console.log(post);
    return res.send(post);

  } catch (e) {
    next(e);
  }
});





module.exports = router;