const mongoose = require('mongoose');
const config = require("./config");
const User = require("./models/User");
const Post = require("./models/Post");
const Comment = require("./models/Comment");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [Saiko, John, Paul] = await User.create({
    email: 'saika95@gmail.com',
    password: '1234',
    name: 'Saiko',
    token: '1234abc'
  }, {
    email: 'ank15@mail.ru',
    password: '1789',
    name: 'John',
    token: '1235abc'
  }, {
    email: 'crez@mail.ru',
    password: '8990',
    name: 'Paul',
    token: '12310abc'
  });

 const [GreatWeatherToday, TheNewsfromBillBoardAward, TheNewDirector] = await Post.create({
    user: Saiko,
    title: 'Great weather today',
    description: "The stock market has dropped down by 50%",
    image: "stockmarket.png",
    date: "10.05.2022"
  }, {
    user: John,
    title: 'The news from BillBoard Award',
    description: "The winners of Bill Board Awards are going to be announced today",
    image: "billboard.jpeg",
    date: "10.05.2022"
  }, {
    user: Paul,
    title: 'The new director has been assigned for Google corporation',
    description: "Google stocks have dropped down by 60%",
    image: "google.jpeg",
    date: "10.05.2022"
  });


  await Comment.create({
    user: Saiko,
    post: GreatWeatherToday,
    text: 'The value of tech stocks have decreased to a great extent'

  })

  await mongoose.connection.close();
};

run().catch(e => console.error(e));